const sequelize = require("./database");

const users = require("./models/users");
const tasks = require("./models/tasks");
const subtasks = require("./models/subtasks");

users.hasMany(tasks, {
  foreignKey: {
    unique: "yes",
    allowNull: false,
  },
});
tasks.hasMany(subtasks, {
  foreignKey: {
    unique: "yes",
    allowNull: false,
  },
});

sequelize
  .sync({ force: true })
  .then(res => console.log("success"))
  .catch(console.log);