const Sequelize = require("sequelize");

const sequelize = require("../database");

const tableColumns = {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  title: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: "yes",
  },
  status: {
    type: Sequelize.BOOLEAN,
    allowNull: false,
    defaultValue: 0,
  },
};

module.exports = sequelize.define("tasks", tableColumns);
