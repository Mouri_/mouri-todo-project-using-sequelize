const Sequelize = require("sequelize");

const config = require("./config.json");

const sequelize = new Sequelize(...Object.values(config.seqDBcredentials), config.seqDBconfig);

module.exports = sequelize;