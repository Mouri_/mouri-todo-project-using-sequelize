const router = require("express").Router();
const tasksModel = require("../models/tasks");
const usersModel = require("../models/users");

tasksModel.belongsTo(usersModel);

router.get("/", (req, res) => {
  tasksModel
    .findAll({
      attributes: ["id", "title", "status"],
      include: {
        model: usersModel,
        attributes: ["id", "name", "email"],
      },
    })
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({ error: "Some error while fetching" }));
});

router.get("/:id", (req, res) => {
  tasksModel
    .findAll({
      attributes: ["id", "title", "status"],
      where: {
        userId: req.params.id,
      },
    })
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({ error: "Some error while fetching" }));
});

router.post("/", (req, res) => {
  tasksModel
    .create({ title: req.body.title, userId: req.body.id })
    .then(result => res.status(201).json({ result: "Success" }))
    .catch(err => res.status(409).json({ error: "Task already exists" }));
});

router.put("/:id", async (req, res) => {
  const isAllowed = await auth(req.body.id, req.params.id);
  delete req.body.id;
  if (isAllowed) {
    tasksModel
      .update(req.body, {
        where: {
          id: req.params.id,
        },
      })
      .then(result => res.status(200).json({ result: "Success" }))
      .catch(err => res.status(500).json({ error: "Internal error" }));
  } else res.status(404).json({ error: "Task not found" });
});

router.delete("/:id", async (req, res) => {
  const isAllowed = await auth(req.body.id, req.params.id);
  if (isAllowed) {
    tasksModel
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then(result => res.status(200).json({ result: "success" }))
      .catch(err => res.status(500).json({ error: "Internal error" }));
  } else res.status(404).json({ error: "Task not found" });
});

async function auth(id, reqID) {
  return tasksModel
    .findAll({
      attributes: ["id", "title", "status"],
      where: {
        "$user.id$": id,
      },
      include: {
        model: usersModel,
        attributes: [],
      },
    })
    .then((result, rej) =>
      result.map(value => value.id).includes(parseInt(reqID))
    );
}

module.exports = router;