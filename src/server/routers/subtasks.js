const router = require("express").Router();
const tasksModel = require("../models/tasks");
const subtasksModel = require("../models/subtasks");
const usersModel = require("../models/users");

tasksModel.belongsTo(usersModel);
subtasksModel.belongsTo(tasksModel);

router.get("/", (req, res) => {
  subtasksModel
    .findAll({
      attributes: ["id", "title", "status"],
      include: {
        model: tasksModel,
        attributes: ["id", "title", "status"],
      },
    })
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({ error: "Some error while fetching" }));
});

router.get("/:id", (req, res) => {
  subtasksModel
    .findAll({
      attributes: ["id", "title", "status"],
      where: {
        taskId: req.params.id,
      },
    })
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({ error: "Some error while fetching" }));
});

router.post("/", async (req, res) => {
  if (!req.body.taskId)
    return res.status(400).json({ error: "Please include task ID" });
  const isAllowed = await auth(req.body.id, req.body.taskId);
  delete req.body.id;
  if (isAllowed) {
    subtasksModel
      .create(req.body)
      .then(result => res.status(201).json({ result: "Success" }))
      .catch(err => res.status(409).json({ error: "Task already exists" }));
  } else res.status(401).json({ error: "Unauthorized" });
});

router.put("/:id", async (req, res) => {
  const taskId = await findTaskID(req.params.id);
  if (!taskId) return res.status(404).json({ error: "Sub task not found" });
  const isAllowed = await auth(req.body.id, taskId);
  delete req.body.id;
  if (isAllowed) {
    subtasksModel
      .update(req.body, {
        where: {
          id: req.params.id,
        },
      })
      .then(result => res.status(200).json({ result: "Success" }))
      .catch(err => res.status(500).json({ error: "Internal error" }));
  } else res.status(401).json({ error: "Unauthorized" });
});

router.delete("/:id", async (req, res) => {
  const taskId = await findTaskID(req.params.id);
  if (!taskId) return res.status(404).json({ error: "Sub task not found" });
  const isAllowed = await auth(req.body.id, taskId);
  delete req.body.id;
  if (isAllowed) {
    subtasksModel
      .destroy({
        where: {
          id: req.params.id,
        },
      })
      .then(result => res.status(200).json({ result: "Success" }))
      .catch(err => res.status(500).json({ error: "Internal error" }));
  } else res.status(401).json({ error: "Unauthorized" });
});

async function auth(id, reqID) {
  return tasksModel
    .findAll({
      attributes: ["userId"],
      where: {
        id: reqID,
      },
    })
    .then(result => result[0].userId == id);
}

async function findTaskID(id) {
  return subtasksModel
    .findAll({
      attributes: ["taskId"],
      where: {
        id: id,
      },
    })
    .then(result => result[0] && result[0].taskId);
}

module.exports = router;
