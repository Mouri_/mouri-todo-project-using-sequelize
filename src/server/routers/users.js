const router = require("express").Router();
const bcrypt = require("bcrypt");
const usersModel = require("../models/users");

router.get("/", (req, res) => {
  usersModel
    .findAll({
      attributes: ["id", "name", "user_name", "email"],
    })
    .then(result => res.status(200).json(result))
    .catch(err => res.status(500).json({ error: "Some error while fetching" }));
});

router.get("/:id", (req, res) => {
  usersModel
    .findByPk(req.params.id, {
      attributes: ["id", "name", "user_name", "email"],
    })
    .then(result => {
      if (result != null) return res.status(200).json(result);
      res.status(404).json({ error: "User doe not exist" });
    })
    .catch(err => res.status(500).json({ error: "Internal error" }));
});

router.post("/", (req, res) => {
  const body = req.body;
  const salt = bcrypt.genSaltSync();
  req.body.password = bcrypt.hashSync(body.password, salt);

  usersModel
    .create(req.body)
    .then(result => res.status(201).json({ result: "Success" }))
    .catch(err =>
      res
        .status(409)
        .json({ error: "User_name or email already exists or invalid email" })
    );
});

router.put("/:id", (req, res) => {
  if (req.params.id != req.body.id)
    return res.status(403).json({ error: "Forbidden" });

  if (req.body.hasOwnProperty("password")) {
    const salt = bcrypt.genSaltSync();
    req.body.password = bcrypt.hashSync(req.body.password, salt);
  }

  usersModel
    .update(req.body, {
      where: {
        id: req.body.id,
      },
    })
    .then(result => res.status(200).json({ result: "Success" }))
    .catch(err => res.status(500).json({ error: "Some error while updating" }));
});

router.delete("/:id", (req, res) => {
  if (req.body.id != req.params.id)
    return res.status(403).json({ error: "Forbidden" });

  usersModel
    .destroy({
      where: {
        id: req.body.id,
      },
    })
    .then(result => res.status(200).json({ result: "success" }))
    .catch(err => res.status(404).json({ error: "User does not exist" }));
});

module.exports = router;
