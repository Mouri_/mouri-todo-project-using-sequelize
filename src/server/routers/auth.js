const usersModel = require("../models/users");
const router = require("express").Router();

router.post("/", (req, res) => {
  const bcrypt = require("bcrypt");
  const jwt = require("jsonwebtoken");
  const user_name = req.body.user_name;
  const password = req.body.password;

  usersModel
    .findAll({
      attributes: ["id", "password"],
      where: {
        user_name: user_name,
      },
    })
    .then(result => {
      if (result.length <= 0)
        return res.status(401).json({ error: "Wrong user name" });
      const user = result[0];
      if (bcrypt.compareSync(password, user.password)) {
        const userAndID = user_name + "," + user.id;
        const token = jwt.sign(userAndID, process.env.JWT_SECRET);
        res.status(200).json({ token: token });
      } else {
        res.status(401).json({ error: "Wrong password" });
      }
    })
    .catch(err => {
      res.status(500).json({ error: "Something went wrong" });
    });
});

module.exports = router;