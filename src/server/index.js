require("dotenv").config();

require("./database");

const express = require("express");

const jwt = require("jsonwebtoken");

const config = require("./config.json");

const authRoute = require("./routers/auth");

const userRoute = require("./routers/users");

const tasksRoute = require("./routers/tasks");

const subTasksRoute = require("./routers/subtasks");

const PORT = process.env.PORT || config.PORT;

const app = express();

app.use(express.json());

app.use("/login", authRoute);

app.use("/", (req, res, next) => {
  if (!req.headers.hasOwnProperty("authorization")) {
    req.body.user = null;
    req.body.id = null;
    return next();
  }
  const token = req.headers.authorization.replace("Bearer ", "");
  jwt.verify(token, process.env.JWT_SECRET, (err, result) => {
    if (err) return res.status(401).json({ error: "Invalid token" });
    [req.body.user, req.body.id] = result.split(",");
    next();
  });
});

app.use("/users", userRoute);

app.use("/", (req, res, next) => {
  if (!req.body.user) return res.status(401).json({ error: "Unauthorized" });
  else next();
});

app.use("/tasks", tasksRoute);

app.use("/subtasks", subTasksRoute);

app.use("/", (req, res) => {
  res.status(400).json({ error: "Bad request" });
});

app.listen(PORT, () => console.log("Express server running on port", PORT));
